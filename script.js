function validation() {
  var fname = document.getElementById('fname').value;
  var lname = document.getElementById('lname').value;
  var password = document.getElementById('password').value;
  var Cpass = document.getElementById('Cpass').value;
  var email = document.getElementById('email').value;
  var phone = document.getElementById('phone').value;
  var demo1 = document.getElementById('demo1');
  var demo2 = document.getElementById('demo2');
  var demo3 = document.getElementById('demo3');
  var demo4 = document.getElementById('demo4');
  var demo5 = document.getElementById('demo5');
  var demo6 = document.getElementById('demo6');
  var check = /^([A-Za-z0-9_\-\.]){1,}\@([A-Za-z0-9_\-\.]){1,}\.([A-Za-z]{2,4})$/;
  if (fname == "") {
    demo1.textContent = "First Name is required !";
    return false;
  }
  if (lname == "") {
    demo2.textContent = "User Name is required !";
    return false;
  }
  if (password == "") {
    demo3.textContent = "Password  is required !";
    return false;
  }
  if (password != Cpass) {
    demo4.textContent = "Password Dose Not Match !";
    return false;
  }
  if (phone == "") {
    demo5.textContent = "Phone Number is required !";
    return false;
  }
  if (isNaN(phone) || phone.length != 10) {
    demo5.textContent = "Please Enter A Number For Your Phone with 10 Digts !";
    return false;
  }
  if (check.test(email) == false) {
    demo6.textContent = "Not A Valid Email !";
    return false;
  }
}
