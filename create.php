<?php
session_start();
$userName= $_SESSION['userName'];
$folderName="";
$type=$_POST['type'];


//folder
if ($type=='folder') {
    $folderName=$_POST['folderName'];
    if (file_exists('files/'.$userName.'/'.$folderName)) {
        echo '<script>alert("The Folder You Want To Creat Is Already Exists! Try Diffranet Name");</script>';
        echo("<script>window.location = 'user.php';</script>");
    } else {
        mkdir("files/".$userName."/".$folderName);

        echo '<script>alert("Folder Created");</script>';
        echo("<script>window.location = 'user.php';</script>");
    }
}

//file
elseif ($type=='file') {
    # code...
    $fileName=$_POST['fileName'];
    $folderName=$_POST['hidden_folder_name'];
    $extention=$_POST['extention'];
    $ourFileName = $fileName.".".$extention;


    if (!file_exists($folderName.'/'.$ourFileName)) {
        $ourFileHandle = fopen($folderName."/".$ourFileName, 'w') or die("can't open file");
        fclose($ourFileHandle);
        echo '<script>alert("file Created");</script>';
        echo("<script>window.location = 'user.php';</script>");
    } else {
        echo '<script>alert("File Already Exists");</script>';
  echo("<script>window.location = 'user.php';</script>");
    }
}

//upload

elseif ($type=='upload') {
    $name=$_FILES['file']['name'];
    $size=$_FILES['file']['size'];

    $tmp_name=$_FILES['file']['tmp_name'];
    $target='files/'.$userName.'/'.'Uploaded Files'.'/';
    $targetFile=$target.$name;
      $fileType=strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));

    if (!empty($name)) {
      //check if file Exists
        if (!file_exists($targetFile)) {
          //check if file is large
          if($size<500000){
            if($fileType=='txt' || $fileType=='jpg' || $fileType=='pptx' || $fileType=='xlsx' || $fileType=='docx' || $fileType=='pdf' || $fileType=='png' || $fileType=='jpeg' || $fileType=='gif' ){
              if(move_uploaded_file($tmp_name,$targetFile)){
                echo '<script>alert("File Uploaded");</script>';
                echo("<script>window.location = 'user.php';</script>");
              }

              }
              else{
                echo '<script>alert("File Type Not Allowed");</script>';
                echo("<script>window.location = 'user.php';</script>");
            }


          }
          else {
            echo '<script>alert("File TOO Large");</script>';
            echo("<script>window.location = 'user.php';</script>");
          }
        } else {
            echo '<script>alert("File Already Exists");</script>';
            echo("<script>window.location = 'user.php';</script>");
        }
    } else {
        echo '<script>alert("Please Chose A File");</script>';
        echo("<script>window.location = 'user.php';</script>");
    }
}

elseif ($type=='creatfile') {
  $fileName=$_POST['fileName'];
  $extention=$_POST['extention'];
  $folderName=$_POST['hidden_folder_name'];
  $ourFileName = $fileName.".".$extention;
  if (!file_exists('files/'.$userName.'/'.$folderName.'/'.$ourFileName)) {
      $ourFileHandle = fopen("files/".$userName."/".$folderName.'/'.$ourFileName, 'w') or die("can't open file");
      fclose($ourFileHandle);
      echo '<script>alert("file Created");</script>';
      echo("<script>window.location = 'user.php';</script>");
  } else {
      echo '<script>alert("File Already Exists");</script>';
      echo("<script>window.location = 'user.php';</script>");
  }
}
